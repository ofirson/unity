﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void StartGameButtonHandler()
    {
        Debug.Log("Lets Play !!");
        SceneManager.LoadScene("HelloVR");
        //move to the user to the game scene
    }
    public void ExitGameButtonHandler()
    {
        Debug.Log("Good Bye !!");
        Application.Quit();
        //move to the user to the game scene
    }

}


