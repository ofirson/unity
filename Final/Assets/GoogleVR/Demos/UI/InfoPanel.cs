﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfoPanel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.SetActive(false);
    }

    public void OnInfoButtonStart()
    {
        this.gameObject.SetActive(true);
    }

    public void HideInfoPanel()
    {
        this.gameObject.SetActive(false);
    }
      

  
}
