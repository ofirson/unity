﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour
{
    public int timeLeft = 10; //Seconds Overall
    public Text startText; //UI Text Object
    public int shootingCounter; 
    void Start()
    {
        StartCoroutine("LoseTime");
        Time.timeScale = 1; //Just making sure that the timeScale is right
    }
    void Update()
    {
        if (timeLeft > 0)
        {
            startText.text = ("" + timeLeft); //Showing the Score on the Canvas
        }
        else
        {
            if (shootingCounter < 3)
            {
                SceneManager.LoadScene("GameOverLoserScene");
            }
            else
            {
                SceneManager.LoadScene("GameOverWinnerScene");
            }

        }
    }
    //Simple Coroutine
    IEnumerator LoseTime()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);
            timeLeft--;
        }

    }
}