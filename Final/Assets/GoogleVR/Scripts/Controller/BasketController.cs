﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BasketController : MonoBehaviour
{
    public GameObject score; //reference to the ScoreText gameobject, set in editor
    public AudioClip basket; //reference to the basket sound
    public Timer m_Timer;
    public Text m_score;

    void start()
    {
        m_Timer.shootingCounter = 0; 
    }

    void OnTriggerEnter(Collider collider) //if ball hits basket collider
    {
        if (collider.name.Contains("Basketball"))
        {
            m_Timer.shootingCounter++;
            m_score.text = ("" + m_Timer.shootingCounter);
            Debug.Log("YAYYY");
        }

        /*int currentScore = int.Parse(score.GetComponent().text) + 1; //add 1 to the score
        score.GetComponent().text = currentScore.ToString();
        AudioSource.PlayClipAtPoint(basket, transform.position); //play basket sound*/
    }
}
