﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public float forceX;
    public float forceZ;
    public GameObject court;

    private Vector3 initialPosition;
    private float timer = 0.0f;
    private bool isThrown = false;

    // Start is called before the first frame update
    void Start()
    {
        initialPosition = this.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        var rb = this.GetComponent<Rigidbody>();
        timer += Time.deltaTime;

        if ((transform.position.y < -10 || timer > 4) && isThrown)
        {
            ResetBall();
            return;
        }

        if (!isThrown && Input.anyKeyDown) //ball doesn't move
        {
            timer = 0;
            rb.useGravity = true;
            
            rb.AddForce(0, forceX, forceZ, ForceMode.Impulse);
            isThrown = true;
            transform.parent = court.transform;
        }

    }


    void ResetBall()
    {
        var rb = this.GetComponent<Rigidbody>();
        rb.useGravity = false;
        rb.velocity = rb.angularVelocity = Vector3.zero;
        transform.position = initialPosition;
        timer = 0;
        isThrown = false;
        transform.parent = Camera.main.transform;
    }

}
