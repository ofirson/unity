﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class LineCollisionController : MonoBehaviour
{
    public float factor = 0.3f;
    private GameObject life;
    private void OnCollisionEnter(Collision collision)
    {

        foreach (ContactPoint cp in collision.contacts)
        {
            //enemy hit
            if (cp.otherCollider.tag == "Enemy")
            {
                if (gameObject.tag == "temp")
                {
                    loseLife();
                }
            }

            //line hit
            if (cp.otherCollider.name.Contains("Line") && cp.otherCollider.tag == "temp")
            {
                //check if not false alarm (turnning)
                if (LineDrawController.points.Count > 0)
                {
                    var point = LineDrawController.points[LineDrawController.points.Count - 1];
                    if ((point - cp.point).magnitude > factor)
                    {
                       // loseLife();
                    }
                }
            }
        }
    }



    private void gameOver()
    {
        SceneManager.LoadScene("GameOverScene");
    }

    private void wonGame()
    {
        SceneManager.LoadScene("GameOverWinnerScene");
    }

    private void loseLife()
    {
        if(life = GameObject.Find("HeartL"))
        {
            Destroy(life);
        }

        else if (life = GameObject.Find("HeartM"))
        {
            Destroy(life);
        }

        else if (life = GameObject.Find("HeartR"))
        {
            Destroy(life);
            gameOver();
        }
    }
}