﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float factor = 0.05f;
    private LineDrawController lineDrawer;
    private bool start = false;

    // Start is called before the first frame update
    void Start()
    {
        lineDrawer = GameObject.Find("Floor").GetComponent<LineDrawController>();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (start)
        {
            GetComponent<Rigidbody>().useGravity = false;

            Vector3 direction;
            //set direction by key
            if (Input.GetKey(KeyCode.UpArrow)) direction = Vector3.forward;
            else if (Input.GetKey(KeyCode.DownArrow)) direction = Vector3.back;
            else if (Input.GetKey(KeyCode.RightArrow)) direction = Vector3.right;
            else if (Input.GetKey(KeyCode.LeftArrow)) direction = Vector3.left;
            else return;

            transform.position += factor * direction;
        }
    }


    private void OnCollisionEnter(Collision collision)
    {
        
        foreach (ContactPoint cp in collision.contacts)
        {
            //wall hit
            if (cp.otherCollider.name.Contains("Wall") || cp.otherCollider.name.Contains("MiddleCube")
                || (cp.otherCollider.tag == "Untagged" && cp.otherCollider.name.Contains("Line")))
            {
                start = true;

                lineDrawer.isOut = false;
                lineDrawer.FinishTurn();
            }
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        //wall hit
        if (collision.gameObject.name.Contains("Wall") || collision.gameObject.name.Contains("MiddleCube")
            || (collision.gameObject.tag == "Untagged" && collision.gameObject.name.Contains("Line")))
        {
            lineDrawer.isOut = true;
            lineDrawer.origin = new Vector3(transform.position.x, 0.2f, transform.position.z);
        }
    }
}
