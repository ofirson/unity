﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawController : MonoBehaviour
{

    public Material wallMaterial;
    public Transform prefab;
    public float factor = 0.05f;
    public Vector3 origin;

    public static List<Vector3> points = new List<Vector3>();
    public bool isOut = false;

    private static List<Transform> lines = new List<Transform>();
    private Vector3 lastDirection = Vector3.positiveInfinity;

    // Start is called before the first frame update
    void Start()
    {
        FinishTurn();
    }

    // Update is called once per frame
    void Update()
    {
        if (isOut)
        {
            Vector3 direction;
            //set direction by key
            if (Input.GetKey(KeyCode.UpArrow)) direction = Vector3.forward;
            else if (Input.GetKey(KeyCode.DownArrow)) direction = Vector3.back;
            else if (Input.GetKey(KeyCode.RightArrow)) direction = Vector3.right;
            else if (Input.GetKey(KeyCode.LeftArrow)) direction = Vector3.left;
            else return;

            //if not moving on same axis, create new dotted line
            if (lastDirection != direction && lastDirection != -1 * direction)
            {
                var tmp = Instantiate(prefab, new Vector3(1, 0, 0), Quaternion.identity);
                tmp.tag = "temp";
                tmp.position = origin + new Vector3(0.0001f, 0, 0.0001f);
                tmp.gameObject.layer = 2; // "Ignore Raycast"
                lines.Add(tmp);

                lastDirection = direction;
                points.Add(origin);
            }

            //scale last line
            var line = lines[lines.Count - 1];
            line.transform.localScale += factor * direction;
            line.transform.position += factor / 2 * direction;

            //update current location and farest spot
            origin += factor * direction;
        }
    }



    public void FinishTurn()
    {
        if (lines.Count == 1 && lines[0].transform.localScale.magnitude < 2) return;

        foreach (Transform line in lines)
        {
            line.tag = "Untagged";
            line.gameObject.layer = 0; //default, not ignoring ray cast
            line.gameObject.GetComponent<MeshRenderer>().material = wallMaterial;
        }
        lines = new List<Transform>();
        points = new List<Vector3>();
        lastDirection = Vector3.positiveInfinity;
    }

    void OnDestroy()
    {
        Debug.Log("OnDestroy1");
        lines = new List<Transform>();
        points = new List<Vector3>();
    }
}