﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    public float Inital_Force;

    [SerializeField]
    Rigidbody rigidbody;

    // Start is called before the first frame update
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.AddForce(5, 0, Inital_Force, ForceMode.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        if (IsDead()) Destroy(gameObject);
        rigidbody.velocity = NormalizeVelocity(rigidbody.velocity);
    }

    private bool IsDead()
    {
        RaycastHit hit;
        Vector3 fromPosition = transform.position;

        foreach (GameObject enemy in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            if (enemy == this) continue;

            Vector3 toPosition = enemy.transform.position;
            Vector3 direction = toPosition - fromPosition;

            Debug.DrawRay(transform.position, direction, Color.green);

            if (Physics.Raycast(transform.position, direction, out hit))
            {
                if (hit.collider.gameObject.tag == "Enemy")
                    return false;
            }

        }

        Debug.Log(gameObject.name + " dead ");
        return true;
    }

    private Vector3 NormalizeVelocity(Vector3 v)
    {
        return new Vector3(NormalizeVelocityHelper(v.x),
            //0,
            NormalizeVelocityHelper(v.y),
            NormalizeVelocityHelper(v.z));
    }

    private float NormalizeVelocityHelper(float x)
    {
        int sign = x < 0 ? -1 : 1;
        if (System.Math.Abs(x) > 5) return sign*5f;
        if (System.Math.Abs(x) < 2) return sign * 2f;
        return x;
    }
}
