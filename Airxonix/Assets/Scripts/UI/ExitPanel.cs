﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitPanel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        this.gameObject.SetActive(false); 
    }

    // Update is called once per frame

    public void ShowExitPanel()
    {
        this.gameObject.SetActive(true);
    }

    public void ExitGame()
    {
       Application.Quit();
    }

    public void Cancel()
    {
        this.gameObject.SetActive(false); 
    }
}
