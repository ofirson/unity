﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class click : MonoBehaviour
{
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                newplane vcp = hit.collider.gameObject.GetComponent<newplane>();
                if (vcp != null)
                    vcp.ChangeColor(hit.triangleIndex);
            }
        }
    }
}