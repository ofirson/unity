﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineCollisionController : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {

        foreach (ContactPoint cp in collision.contacts)
        {
            if (cp.otherCollider.name == "Line")
            {
                var point = LineDrawController.points[LineDrawController.points.Count - 1];
                if ((point - cp.point).magnitude > 0.1f)
                {
                    Debug.Log("colision");
                }
            }
        }
    }

}