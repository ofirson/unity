﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Plane : MonoBehaviour
{

    public Vector3 contactPoint = Vector3.zero;
    public Vector3 nearestVert;

    Mesh mesh;
    Vector3[] verts;
    Vector3 vertPos;
    Color32[] colors;
    
    void Start()
    {
        /*mesh = GetComponent<MeshFilter>().mesh;
        verts = mesh.vertices;*/
        //colors = mesh.colors;
        /*colors = new Color[verts.Length];
        for (int i =0;i< verts.Length;i++)
        {
            colors[i] = Color.blue;
        }*/
        // create new colors array where the colors will be created.
        /*colors = new Color32[verts.Length];

        for (int i = 0; i < verts.Length; i++)
        {
            colors[i] = Color.white;
            if (i % 3 == 0)
                colors[i] = new Color(
                                        Random.Range(0.0f, 1.0f),
                                        Random.Range(0.0f, 1.0f),
                                        Random.Range(0.0f, 1.0f),
                                        1.0f);
        }

        // assign the array of colors to the Mesh.
        mesh.colors32 = colors;*/

        Mesh mesh = gameObject.GetComponent<MeshFilter>().mesh;
        int[] triangles = mesh.triangles;
        Vector3[] vertices = mesh.vertices;
        Vector3[] verticesModified = new Vector3[triangles.Length];
        int[] trianglesModified = new int[triangles.Length];
        Color32 currentColor = Color.cyan;
        Color32[] colors = new Color32[triangles.Length];
        for (int i = 0; i < trianglesModified.Length; i++)
        {
            // Makes every vertex unique
            verticesModified[i] = vertices[triangles[i]];
            trianglesModified[i] = i;
            // Every third vertex randomly chooses new color
            colors[triangles[i]] = currentColor;
            if (true || i % 2 == 0)
            {
                currentColor = new Color(
                                    Random.Range(0.0f, 1.0f),
                                    Random.Range(0.0f, 1.0f),
                                    Random.Range(0.0f, 1.0f),
                                    1.0f);
                colors[triangles[i]] = currentColor;
            }
        }
        // Applyes changes to mesh
        mesh.vertices = verticesModified;
        mesh.triangles = trianglesModified;
        mesh.colors32 = colors;
    }

    void Update()
    {
        //colors[NearestVertexTo(contactPoint)] = Color.black;
        //mesh.colors32 = colors;

    }

    int NearestVertexTo(Vector3 point)
    {
        point = transform.InverseTransformPoint(point);

        float minDistanceSqr = Mathf.Infinity;
        int nearestVertex = -1;

        // scan all vertices to find nearest
        for (int i = 0; i < verts.Length; i++)
        {
            float distSqr = (point - verts[i]).sqrMagnitude;

            if (distSqr < minDistanceSqr)
            {
                minDistanceSqr = distSqr;
                nearestVertex = i;
            }
        }
        return nearestVertex;
    }

    void OnCollisionStay(Collision collision)
    {
        /*foreach (ContactPoint contact in collision.contacts)
        {
            Debug.DrawRay(contact.point, Vector3.up, Color.green, 4, false);
            contactPoint = contact.point;
        }*/
    }
}