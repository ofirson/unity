﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class LineDrawController : MonoBehaviour
{
    public Transform prefab;
    public float factor = 0.05f;
    public Vector3 origin;
    public static List<Vector3> points = new List<Vector3>();

    private List<Transform> lines = new List<Transform>();
    private Vector3 lastDirection = Vector3.positiveInfinity;
    private Vector3 farestSpot;

    // Start is called before the first frame update
    void Start()
    {
        farestSpot = origin;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 direction;
        //set direction by key
        if (Input.GetKey(KeyCode.UpArrow)) direction = Vector3.forward;
        else if (Input.GetKey(KeyCode.DownArrow)) direction = Vector3.back;
        else if (Input.GetKey(KeyCode.RightArrow)) direction = Vector3.right;
        else if (Input.GetKey(KeyCode.LeftArrow)) direction = Vector3.left;
        else return;

        //adjust direction if pointing backwards
        if (farestSpot.z > origin.z)
        {
            if (Input.GetKey(KeyCode.RightArrow)) direction = Vector3.left;
            else if (Input.GetKey(KeyCode.LeftArrow)) direction = Vector3.right;
        }

        //if not moving on same axis, create new dotted line
        if (lastDirection != direction && lastDirection != -1 * direction)
        {
            var tmp = Instantiate(prefab, new Vector3(1, 0.3f, 0), Quaternion.identity);
            tmp.position = origin + new Vector3(0.0001f, 0, 0.0001f);
            //if (Input.GetKey(KeyCode.RightArrow)) tmp.Rotate(90,0,0);
            lines.Add(tmp);

            lastDirection = direction;
            points.Add(origin);
        }

        //scale last line
        var line = lines[lines.Count - 1];
        line.transform.localScale += factor * direction;
        line.transform.position += factor / 2 * direction;

        //update current location and farest spot
        origin += factor * direction;
        if (farestSpot.z < origin.z)
        {
            farestSpot = origin;
        }

    }

}